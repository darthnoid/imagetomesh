﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Converter;
using System.IO;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;

namespace ImageToMesh
{
    delegate List<Vertex> DoConversionDelegate(string fileName, ProgressBar progress = null);
    delegate void UpdateLabelComplete();
    delegate int AddKeyToCache(object key);
    delegate void ConversionCompleteDelegate();

    public struct CacheKey
    {
        public string FilePath { get; set; }
        public float XScale { get; set; }
        public float YScale { get; set; }
        public float ZScale { get; set; }
    }

    public partial class MainForm : Form
    {
        Converter.Converter MeshConverter = new Converter.Converter();
        bool loading = false;
        bool glLoaded = false;
        List<Vertex> verts = null;

        Dictionary<CacheKey, List<Vertex>> fileDataCache = new Dictionary<CacheKey, List<Vertex>>();

        public MainForm()
        {
            InitializeComponent();
            loadedLabel.Text = string.Empty;
            imagePreview.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        public void SetLabelToLoaded(IAsyncResult result)
        {
            loading = false;

            CacheKey key = new CacheKey();
            key.FilePath = Path.GetFullPath(fileTextbox.Text);
            key.XScale = (float)widthNumeric.Value;
            key.YScale = (float)heightNumeric.Value;
            key.ZScale = (float)depthNumeric.Value;

            UpdateLabelComplete upd = new UpdateLabelComplete(UpdateText);
            AddKeyToCache updateCache = new AddKeyToCache(listBox.Items.Add);
            fileDataCache.Add(key, new List<Vertex>());
            loadedLabel.BeginInvoke(upd, null);
            listBox.BeginInvoke(updateCache, fileTextbox.Text);
        }

        public void UpdateText()
        {
            loadedLabel.Text = "Loaded!";
        }

        #region event handlers
        private void browseButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialogue = new OpenFileDialog();
            dialogue.Filter = "BMP| *.bmp| PNG| *.png| All| *.*";
            var x = dialogue.ShowDialog();

            if (!String.IsNullOrWhiteSpace(dialogue.FileName))
            {
                fileTextbox.Text = dialogue.FileName;
                string fullPath = Path.GetFullPath(fileTextbox.Text);
                imagePreview.ImageLocation = fullPath;
            }
        }

        private void exportToOBJToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExportForm exportForm = new ExportForm();
            exportForm.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            string fileName = fileTextbox.Text;

            if (String.IsNullOrWhiteSpace(fileName) || !File.Exists(fileName) || loading)
            {
                return;
            }

            fileName = Path.GetFullPath(fileName);

            loading = true;
            loadedLabel.Text = String.Empty;

            imagePreview.ImageLocation = fileName;

            CacheKey key = new CacheKey();
            key.FilePath = fileName;
            key.XScale = (float)widthNumeric.Value;
            key.YScale = (float)heightNumeric.Value;
            key.ZScale = (float)depthNumeric.Value;

            if (fileDataCache.ContainsKey(key))
            {
                verts = fileDataCache[key];
                loading = false;
                loadedLabel.Text = "Loaded!";
                return;
            }

            AsyncCallback callback = new AsyncCallback(SetLabelToLoaded);

            MeshConverter.XScale = (float)widthNumeric.Value;
            MeshConverter.YScale = (float)heightNumeric.Value;
            MeshConverter.ZScale = (float)depthNumeric.Value;
            DoConversionDelegate doConversion = new DoConversionDelegate(MeshConverter.Convert);
            progressBar1.Value = 0;
            doConversion.BeginInvoke(fileName, progressBar1, callback, null);
        }
        #endregion

        private void configurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConfigForm form = new ConfigForm();
            form.ShowDialog();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void glView_Load(object sender, EventArgs e)
        {
            glLoaded = true;
        }

        private void glView_Paint(object sender, PaintEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            glView.SwapBuffers();
        }
    }
}
