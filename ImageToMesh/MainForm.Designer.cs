﻿namespace ImageToMesh
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.fileTextbox = new System.Windows.Forms.TextBox();
            this.loadButton = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.loadedLabel = new System.Windows.Forms.Label();
            this.widthLabel = new System.Windows.Forms.Label();
            this.depthLabel = new System.Windows.Forms.Label();
            this.heightLabel = new System.Windows.Forms.Label();
            this.browseButton = new System.Windows.Forms.Button();
            this.widthNumeric = new System.Windows.Forms.NumericUpDown();
            this.depthNumeric = new System.Windows.Forms.NumericUpDown();
            this.heightNumeric = new System.Windows.Forms.NumericUpDown();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToOBJToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listBox = new System.Windows.Forms.ListBox();
            this.imagePreview = new System.Windows.Forms.PictureBox();
            this.texWidthLabel = new System.Windows.Forms.Label();
            this.textWidthNumeric = new System.Windows.Forms.NumericUpDown();
            this.texHeightLabel = new System.Windows.Forms.Label();
            this.texHeightNumeric = new System.Windows.Forms.NumericUpDown();
            this.noiseButton = new System.Windows.Forms.Button();
            this.saveNoiseButton = new System.Windows.Forms.Button();
            this.glView = new OpenTK.GLControl();
            ((System.ComponentModel.ISupportInitialize)(this.widthNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.depthNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightNumeric)).BeginInit();
            this.mainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imagePreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWidthNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.texHeightNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // fileTextbox
            // 
            this.fileTextbox.Location = new System.Drawing.Point(12, 27);
            this.fileTextbox.Name = "fileTextbox";
            this.fileTextbox.Size = new System.Drawing.Size(270, 20);
            this.fileTextbox.TabIndex = 0;
            // 
            // loadButton
            // 
            this.loadButton.Location = new System.Drawing.Point(347, 27);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(55, 20);
            this.loadButton.TabIndex = 2;
            this.loadButton.Text = "Load";
            this.loadButton.UseVisualStyleBackColor = true;
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 415);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(385, 23);
            this.progressBar1.TabIndex = 2;
            // 
            // loadedLabel
            // 
            this.loadedLabel.AutoSize = true;
            this.loadedLabel.Location = new System.Drawing.Point(12, 399);
            this.loadedLabel.Name = "loadedLabel";
            this.loadedLabel.Size = new System.Drawing.Size(46, 13);
            this.loadedLabel.TabIndex = 3;
            this.loadedLabel.Text = "Loaded!";
            this.loadedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // widthLabel
            // 
            this.widthLabel.AutoSize = true;
            this.widthLabel.Location = new System.Drawing.Point(4, 107);
            this.widthLabel.Name = "widthLabel";
            this.widthLabel.Size = new System.Drawing.Size(71, 13);
            this.widthLabel.TabIndex = 4;
            this.widthLabel.Text = "Width Scalar:";
            // 
            // depthLabel
            // 
            this.depthLabel.AutoSize = true;
            this.depthLabel.Location = new System.Drawing.Point(135, 107);
            this.depthLabel.Name = "depthLabel";
            this.depthLabel.Size = new System.Drawing.Size(72, 13);
            this.depthLabel.TabIndex = 6;
            this.depthLabel.Text = "Depth Scalar:";
            // 
            // heightLabel
            // 
            this.heightLabel.AutoSize = true;
            this.heightLabel.Location = new System.Drawing.Point(267, 107);
            this.heightLabel.Name = "heightLabel";
            this.heightLabel.Size = new System.Drawing.Size(74, 13);
            this.heightLabel.TabIndex = 8;
            this.heightLabel.Text = "Height Scalar:";
            // 
            // browseButton
            // 
            this.browseButton.Location = new System.Drawing.Point(288, 27);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(55, 20);
            this.browseButton.TabIndex = 1;
            this.browseButton.Text = "Browse";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // widthNumeric
            // 
            this.widthNumeric.DecimalPlaces = 2;
            this.widthNumeric.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.widthNumeric.Location = new System.Drawing.Point(81, 105);
            this.widthNumeric.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            65536});
            this.widthNumeric.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.widthNumeric.Name = "widthNumeric";
            this.widthNumeric.Size = new System.Drawing.Size(48, 20);
            this.widthNumeric.TabIndex = 7;
            this.widthNumeric.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            // 
            // depthNumeric
            // 
            this.depthNumeric.DecimalPlaces = 2;
            this.depthNumeric.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.depthNumeric.Location = new System.Drawing.Point(213, 105);
            this.depthNumeric.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            65536});
            this.depthNumeric.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.depthNumeric.Name = "depthNumeric";
            this.depthNumeric.Size = new System.Drawing.Size(48, 20);
            this.depthNumeric.TabIndex = 8;
            this.depthNumeric.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            // 
            // heightNumeric
            // 
            this.heightNumeric.DecimalPlaces = 2;
            this.heightNumeric.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.heightNumeric.Location = new System.Drawing.Point(347, 105);
            this.heightNumeric.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            65536});
            this.heightNumeric.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.heightNumeric.Name = "heightNumeric";
            this.heightNumeric.Size = new System.Drawing.Size(48, 20);
            this.heightNumeric.TabIndex = 9;
            this.heightNumeric.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            // 
            // mainMenu
            // 
            this.mainMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.mainMenu.Size = new System.Drawing.Size(834, 24);
            this.mainMenu.TabIndex = 14;
            this.mainMenu.Text = "mainMenu";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportToOBJToolStripMenuItem,
            this.configurationToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // exportToOBJToolStripMenuItem
            // 
            this.exportToOBJToolStripMenuItem.Name = "exportToOBJToolStripMenuItem";
            this.exportToOBJToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.exportToOBJToolStripMenuItem.Text = "Export To OBJ";
            this.exportToOBJToolStripMenuItem.Click += new System.EventHandler(this.exportToOBJToolStripMenuItem_Click);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.configurationToolStripMenuItem.Text = "Configuration";
            this.configurationToolStripMenuItem.Click += new System.EventHandler(this.configurationToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // listBox
            // 
            this.listBox.FormattingEnabled = true;
            this.listBox.Location = new System.Drawing.Point(7, 132);
            this.listBox.Name = "listBox";
            this.listBox.Size = new System.Drawing.Size(117, 264);
            this.listBox.TabIndex = 10;
            // 
            // imagePreview
            // 
            this.imagePreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imagePreview.Location = new System.Drawing.Point(130, 132);
            this.imagePreview.Name = "imagePreview";
            this.imagePreview.Size = new System.Drawing.Size(264, 264);
            this.imagePreview.TabIndex = 16;
            this.imagePreview.TabStop = false;
            // 
            // texWidthLabel
            // 
            this.texWidthLabel.AutoSize = true;
            this.texWidthLabel.Location = new System.Drawing.Point(90, 58);
            this.texWidthLabel.Name = "texWidthLabel";
            this.texWidthLabel.Size = new System.Drawing.Size(74, 13);
            this.texWidthLabel.TabIndex = 17;
            this.texWidthLabel.Text = "Texture Width";
            // 
            // textWidthNumeric
            // 
            this.textWidthNumeric.Location = new System.Drawing.Point(173, 55);
            this.textWidthNumeric.Maximum = new decimal(new int[] {
            2056,
            0,
            0,
            0});
            this.textWidthNumeric.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.textWidthNumeric.Name = "textWidthNumeric";
            this.textWidthNumeric.Size = new System.Drawing.Size(66, 20);
            this.textWidthNumeric.TabIndex = 5;
            this.textWidthNumeric.ThousandsSeparator = true;
            this.textWidthNumeric.Value = new decimal(new int[] {
            64,
            0,
            0,
            0});
            // 
            // texHeightLabel
            // 
            this.texHeightLabel.AutoSize = true;
            this.texHeightLabel.Location = new System.Drawing.Point(90, 83);
            this.texHeightLabel.Name = "texHeightLabel";
            this.texHeightLabel.Size = new System.Drawing.Size(77, 13);
            this.texHeightLabel.TabIndex = 19;
            this.texHeightLabel.Text = "Texture Height";
            // 
            // texHeightNumeric
            // 
            this.texHeightNumeric.Location = new System.Drawing.Point(173, 81);
            this.texHeightNumeric.Maximum = new decimal(new int[] {
            2056,
            0,
            0,
            0});
            this.texHeightNumeric.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.texHeightNumeric.Name = "texHeightNumeric";
            this.texHeightNumeric.Size = new System.Drawing.Size(66, 20);
            this.texHeightNumeric.TabIndex = 6;
            this.texHeightNumeric.ThousandsSeparator = true;
            this.texHeightNumeric.Value = new decimal(new int[] {
            64,
            0,
            0,
            0});
            // 
            // noiseButton
            // 
            this.noiseButton.Location = new System.Drawing.Point(12, 53);
            this.noiseButton.Name = "noiseButton";
            this.noiseButton.Size = new System.Drawing.Size(72, 23);
            this.noiseButton.TabIndex = 3;
            this.noiseButton.Text = "Get Noise";
            this.noiseButton.UseVisualStyleBackColor = true;
            // 
            // saveNoiseButton
            // 
            this.saveNoiseButton.Location = new System.Drawing.Point(12, 78);
            this.saveNoiseButton.Name = "saveNoiseButton";
            this.saveNoiseButton.Size = new System.Drawing.Size(72, 23);
            this.saveNoiseButton.TabIndex = 4;
            this.saveNoiseButton.Text = "Save Noise";
            this.saveNoiseButton.UseVisualStyleBackColor = true;
            // 
            // glView
            // 
            this.glView.BackColor = System.Drawing.Color.Black;
            this.glView.Location = new System.Drawing.Point(408, 27);
            this.glView.Name = "glView";
            this.glView.Size = new System.Drawing.Size(414, 411);
            this.glView.TabIndex = 23;
            this.glView.VSync = false;
            this.glView.Load += new System.EventHandler(this.glView_Load);
            this.glView.Paint += new System.Windows.Forms.PaintEventHandler(this.glView_Paint);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 441);
            this.Controls.Add(this.glView);
            this.Controls.Add(this.saveNoiseButton);
            this.Controls.Add(this.noiseButton);
            this.Controls.Add(this.texHeightNumeric);
            this.Controls.Add(this.texHeightLabel);
            this.Controls.Add(this.textWidthNumeric);
            this.Controls.Add(this.texWidthLabel);
            this.Controls.Add(this.imagePreview);
            this.Controls.Add(this.listBox);
            this.Controls.Add(this.heightNumeric);
            this.Controls.Add(this.depthNumeric);
            this.Controls.Add(this.widthNumeric);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.heightLabel);
            this.Controls.Add(this.depthLabel);
            this.Controls.Add(this.widthLabel);
            this.Controls.Add(this.loadedLabel);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.loadButton);
            this.Controls.Add(this.fileTextbox);
            this.Controls.Add(this.mainMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mainMenu;
            this.MaximumSize = new System.Drawing.Size(850, 480);
            this.MinimumSize = new System.Drawing.Size(425, 480);
            this.Name = "MainForm";
            this.Text = "Height Map Converter";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.widthNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.depthNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightNumeric)).EndInit();
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imagePreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWidthNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.texHeightNumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox fileTextbox;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label loadedLabel;
        private System.Windows.Forms.Label widthLabel;
        private System.Windows.Forms.Label depthLabel;
        private System.Windows.Forms.Label heightLabel;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.NumericUpDown widthNumeric;
        private System.Windows.Forms.NumericUpDown depthNumeric;
        private System.Windows.Forms.NumericUpDown heightNumeric;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToOBJToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ListBox listBox;
        private System.Windows.Forms.PictureBox imagePreview;
        private System.Windows.Forms.Label texWidthLabel;
        private System.Windows.Forms.NumericUpDown textWidthNumeric;
        private System.Windows.Forms.Label texHeightLabel;
        private System.Windows.Forms.NumericUpDown texHeightNumeric;
        private System.Windows.Forms.Button noiseButton;
        private System.Windows.Forms.Button saveNoiseButton;
        private OpenTK.GLControl glView;
    }
}

