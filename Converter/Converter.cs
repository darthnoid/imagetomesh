﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Converter
{
    delegate void ShowProgressDelegate(ProgressBar bar);

    public class Converter
    {
        private float _xScale;
        private float _yScale;
        private float _zScale;

        public float XScale 
        {
            get { return _xScale; } 
            set { _xScale = value > 0.0 ? value : _xScale; } 
        }
        public float YScale 
        {
            get { return _yScale; } 
            set { _yScale = value > 0.0 ? value : _yScale; } 
        }
        public float ZScale 
        {
            get { return _zScale; } 
            set { _zScale = value > 0.0 ? value : _zScale; } 
        }

        public Converter()
        {
            _xScale = 0.0f;
            _yScale = 0.0f;
            _zScale = 0.0f;
        }

        public List<Vertex> Convert(string imagePath, ProgressBar progress = null)
        {
            if (!File.Exists(imagePath))
            {
                return null;
            }

            List<Vertex> vertices = new List<Vertex>();

            using (Bitmap img = new Bitmap(imagePath))
            {
                int width = img.Width;
                int height = img.Height;

                int total = width * height;
                int onePercent = (int)((float)total * 0.01f);
                             
                for (int i = 0, current = 0; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        Color tempColor = img.GetPixel(j, i);
                        vertices.Add(GetVertex(j, i, width, height, tempColor));
                        current++;

                        if (progress != null && current % onePercent == 0)
                        {
                            ShowProgressDelegate prog = new ShowProgressDelegate(IncrementBar);
                            progress.BeginInvoke(prog, progress);
                        }
                    }
                }
            }
            return vertices;
        }

        public void IncrementBar(ProgressBar bar)
        {
            bar.Increment(1);
        }

        private Vertex GetVertex(int x, int y, int width, int depth, Color color)
        {
            if (color == null)
            {
                return null;
            }

            Vertex result = new Vertex();
            result.x = x * XScale;
            result.z = y * ZScale; //in most 3D systems y is actually up so if we lay the bitmap flat the "Y" in the BMP is actually the Z
            result.z = ((color.R + color.G + color.B) / 255) * YScale;
            
            return result;
        }
    }
}
