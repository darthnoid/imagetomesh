﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Converter
{
    public class NoiseGenerator
    {
        public NoiseGenerator()
        {
        }

        public void GetNoise(int dimensions, int xResolution, int yResolution)
        {
            float[,] noiseArray = new float[yResolution, xResolution];
        }

        private float Lerp(float a0, float a1, float w)
        {
            return (1.0f - w) * a0 + w * a1;
        }
    }
}
