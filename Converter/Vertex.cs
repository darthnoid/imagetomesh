﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Converter
{    
    public class Vertex
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }

        public Vertex()
        {
            x = 0.0f;
            y = 0.0f;
            z = 0.0f;
        }
    }
}
